module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),

			less: {
				dev: {
					options: {
						config: 'dev/config/yandex.json'
					},
					files: {
						"css/style.css": "dev/less/main.less"
					}
				}
			},

			csscomb: {
				 dev: {
				    files: {
					    "css/style.css": "css/style.css"
				    }
				 }
			},

			watch: {
				css: {
					files: "dev/less/**/*.less",
					tasks: ["less:dev"],
						options: {
						  livereload: true
					}
				}
			}

    });
	
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-csscomb');

	grunt.registerTask('default', ['watch']);
	grunt.registerTask('dev', ['less:dev', 'csscomb:dev']);

};