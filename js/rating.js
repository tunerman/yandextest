/**
 * Плагин для выставление рейтинга
 */

;(function ( $, window, document, undefined ) {

	 var defaults = {
		 value: 0,
		 stars: 5
	 };

	function Plugin( element, options ) {
		this.element = element;
		this.$elem = $(this.element);
		this.options = $.extend( {}, defaults, options );
		this.init();
	}

	Plugin.prototype = {

		init: function() {
			var stars = parseInt(this.$elem.data('stars') || this.options.stars);
			var value = parseInt(this.$elem.data('value') || this.options.value);

			for (var i = 0; i < stars; i++) {
				if (i >= value) {
					this.$elem.append('<i class="rating__star"></i>');
				} else {
					this.$elem.append('<i class="rating__star rating__star--active"></i>');
				}
			}
		}

	};

	$.fn.rating = function ( options ) {

		var make = function() {
			new Plugin( this, options );
			var $this = $(this);
			var stars = $this.find(".rating__star");
			var active_star = stars.filter(".rating__star--active").length;

			stars.on({
				mouseenter: function() {
					var hover_star = $(this);
					stars.each(function() {
						var star = $(this);
						if (hover_star.index() >= star.index()) {
							star.addClass("rating__star--active");
						} else {
							star.removeClass("rating__star--active");
						}
					})
				},
				mouseleave: function () {
					stars.each(function() {
						var star = $(this);
						if (active_star > star.index()) {
							star.addClass("rating__star--active");
						} else {
							star.removeClass("rating__star--active");
						}
					})
				},
        click: function() {
          $this.addClass("rating__star--selected");
          stars.off();
        }
			});
		};

		return this.each(make);
	};

})( jQuery, window, document );