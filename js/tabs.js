/**
 * Плагин для табов
 */
(function( $ ) {

  var methods = {
	init : function( options ) {
		return this.each(function(){
			$(this).bind("click.tab", methods.click);
		});
	},
	show : function( tab, tabactive ) {
		$(tab).show();
		$(tabactive).addClass("tab--active");
	},
	hideAll : function( tabs ) {
		tabs.each(function() {
			$(this).hide();
		});
	},
	resetActive: function(links) {
		links.each(function() {
			$(this).removeClass("tab--active");
		});
	},
	click : function( e ) {
		var $this = $(this);
		var target = $this.attr("href").replace(/.*(?=#[^\s]*$)/, '');

		methods.resetActive($(".tab"));
		methods.hideAll($(".content__tab"));
		methods.show(target, this);
		e.preventDefault();
	}
  };

  $.fn.tabs = function(method) {

		if ( methods[method] ) {
			return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Метод с именем ' +  method + ' не существует.' );
		}

  };
})(jQuery);